package com.openpayd.accounting.business.control;

import com.openpayd.accounting.business.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Client repository.
 */
public interface ClientRepository extends JpaRepository<Client, Long> {
}