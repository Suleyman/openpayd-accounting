package com.openpayd.accounting.business.control;

import com.openpayd.accounting.business.entity.Account;
import com.openpayd.accounting.business.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * The interface Transaction repository.
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    /**
     * Find all by credit account id or debit account id list.
     *
     * @param creditAccountId the credit account id
     * @param debitAccountId  the debit account id
     * @return the list
     */
    List<Transaction> findAllByCreditAccount_IdOrDebitAccount_Id(Long creditAccountId, Long debitAccountId);

}
