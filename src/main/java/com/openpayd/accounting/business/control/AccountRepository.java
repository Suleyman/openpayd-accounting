package com.openpayd.accounting.business.control;

import com.openpayd.accounting.business.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * The interface Account repository.
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

    /**
     * Find all by client id list.
     *
     * @param client_id the client id
     * @return the list
     */
    List<Account> findAllByClient_Id(Long client_id);

}