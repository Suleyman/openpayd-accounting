package com.openpayd.accounting.business.entity;

/**
 * The enum Balance status.
 */
public enum BalanceStatus {

    /**
     * Debit balance status.
     */
    DEBIT,
    /**
     * Credit balance status.
     */
    CREDIT

}
