package com.openpayd.accounting.business.entity;

/**
 * The enum Account type.
 */
public enum AccountType {

    /**
     * Current account type.
     */
    CURRENT,
    /**
     * Savings account type.
     */
    SAVINGS

}
