package com.openpayd.accounting.business.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * The type Client.
 */
@Data
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false, updatable = false, unique = true)
    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Column(nullable = false, updatable = false)
    private String name;

    @NotEmpty(message = "Surname cannot be empty")
    @Column(nullable = false, updatable = false)
    private String surname;

    @NotNull(message = "Address cannot be empty")
    @OneToOne(cascade = CascadeType.ALL)
    private Address primaryAddress;

    @OneToOne(cascade = CascadeType.ALL)
    private Address secondaryAddress;

    /**
     * Instantiates a new Client.
     */
    public Client() {
    }

    /**
     * Instantiates a new Client.
     *
     * @param name             the name
     * @param surname          the surname
     * @param primaryAddress   the primary address
     * @param secondaryAddress the secondary address
     */
    public Client(String name, String surname, Address primaryAddress, Address secondaryAddress) {
        this.name = name;
        this.surname = surname;
        this.primaryAddress = primaryAddress;
        this.secondaryAddress = secondaryAddress;
    }

}
