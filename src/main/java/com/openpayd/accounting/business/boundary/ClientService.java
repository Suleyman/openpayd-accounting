package com.openpayd.accounting.business.boundary;

import com.openpayd.accounting.business.control.ClientRepository;
import com.openpayd.accounting.business.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type Client service.
 */
@Service
public class ClientService extends BaseEntityService {

    @Autowired
    private ClientRepository repository;

    @Override
    public CrudRepository getRepo() {
        return repository;
    }

    /**
     * Create client.
     *
     * @param client the client
     * @return the client
     */
    @Transactional
    public Client create(Client client) {
        return repository.save(client);
    }

    /**
     * Find by id client.
     *
     * @param id the id
     * @return the client
     */
    public Client findById(Long id) {
        return repository.findById(id).orElseThrow(() -> NOT_FOUND_EX);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<Client> findAll() {
        return repository.findAll();
    }

    /**
     * Delete by id.
     *
     * @param id the id
     */
    @Transactional
    public void deleteById(Long id) {
        checkExists(id);
        repository.deleteById(id);
    }

}
