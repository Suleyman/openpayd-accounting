package com.openpayd.accounting.business.boundary;

import com.openpayd.accounting.business.control.TransactionRepository;
import com.openpayd.accounting.business.entity.Account;
import com.openpayd.accounting.business.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

/**
 * The type Transaction service.
 */
@Service
@Transactional
public class TransactionService {

    @Autowired
    private TransactionRepository repository;

    @Autowired
    private AccountService accountService;

    /**
     * Gets account transactions.
     *
     * @param accountId the account id
     * @return Account transactions
     */
    public List<Transaction> getAccountTransactions(Long accountId) {
        return repository.findAllByCreditAccount_IdOrDebitAccount_Id(accountId, accountId);
    }

    /**
     * Make transfer transaction.
     *
     * @param debitAccount  Sender account id
     * @param creditAccount Receiver account id
     * @param amount        Amount
     * @param message       Transfer explaining message
     * @return the transaction
     */
    @Transactional
    public Transaction makeTransfer(Long debitAccount, Long creditAccount, BigDecimal amount, String message) {
        Account sender = accountService.findById(debitAccount);
        Account receiver = accountService.findById(creditAccount);

        try {
            accountService.debitAccount(sender, amount);
            accountService.creditAccount(receiver, amount);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred with the transfer!", e);
        }

        return repository.save(new Transaction(sender, receiver, amount, message));
    }

}
