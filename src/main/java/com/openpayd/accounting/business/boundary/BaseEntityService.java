package com.openpayd.accounting.business.boundary;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * The type Base entity service.
 */
public abstract class BaseEntityService {

    /**
     * The Not found ex.
     */
    static final ResponseStatusException NOT_FOUND_EX = new ResponseStatusException(NOT_FOUND, "Not Found");

    /**
     * Gets repo.
     *
     * @return the repo
     */
    public abstract CrudRepository getRepo();

    /**
     * Check exists.
     *
     * @param id the id
     */
    @SuppressWarnings("unchecked")
    public void checkExists(Long id) {
        if (!getRepo().existsById(id)) {
            throw NOT_FOUND_EX;
        }
    }

}
