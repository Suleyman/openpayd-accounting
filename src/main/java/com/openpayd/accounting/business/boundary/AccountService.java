package com.openpayd.accounting.business.boundary;

import com.openpayd.accounting.business.control.AccountRepository;
import com.openpayd.accounting.business.entity.Account;
import com.openpayd.accounting.business.entity.BalanceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * The type Account service.
 */
@Service
public class AccountService extends BaseEntityService {

    @Autowired
    private AccountRepository repository;

    @Override
    public CrudRepository getRepo() {
        return repository;
    }

    /**
     * Find client accounts list.
     *
     * @param clientId the client id
     * @return the list
     */
    public List<Account> findClientAccounts(Long clientId) {
        return repository.findAllByClient_Id(clientId);
    }

    /**
     * Create a client account.
     *
     * @param account the account
     * @return the account
     */
    @Transactional
    public Account createAccount(Account account) {
        return repository.save(account);
    }

    /**
     * Find by id account.
     *
     * @param id the id
     * @return the account
     */
    public Account findById(Long id) {
        return repository.findById(id).orElseThrow(() -> NOT_FOUND_EX);
    }

    /**
     * Make a DR on account with given amount
     *
     * @param account issued account
     * @param amount  operation amount
     * @return updated account
     */
    @Transactional
    protected Account debitAccount(Account account, BigDecimal amount) {
        if (BalanceStatus.DEBIT.equals(account.getBalanceStatus())) {
            account.setBalance(account.getBalance().add(amount));
        } else {
            if (amount.compareTo(account.getBalance()) > 0) {
                account.setBalance(amount.subtract(account.getBalance()));
                account.setBalanceStatus(BalanceStatus.DEBIT);
            } else {
                account.setBalance(account.getBalance().subtract(amount));
            }
        }
        return repository.save(account);
    }

    /**
     * Make a CR on account with given amount
     *
     * @param account issued account
     * @param amount  operation amount
     * @return updated account
     */
    @Transactional
    protected Account creditAccount(Account account, BigDecimal amount) {
        if (BalanceStatus.CREDIT.equals(account.getBalanceStatus())) {
            account.setBalance(account.getBalance().add(amount));
        } else {
            if (amount.compareTo(account.getBalance()) >= 0) {
                account.setBalanceStatus(BalanceStatus.CREDIT);
                account.setBalance(amount.subtract(account.getBalance()));
            } else {
                account.setBalance(account.getBalance().subtract(amount));
            }
        }
        return repository.save(account);
    }

}
