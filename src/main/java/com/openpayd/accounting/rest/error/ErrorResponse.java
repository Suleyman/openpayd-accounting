package com.openpayd.accounting.rest.error;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

/**
 * The type Error response.
 */
@Data
public class ErrorResponse {

    private HttpStatus httpStatus;
    private String message;
    private List<String> errors;

    /**
     * Instantiates a new Error response.
     *
     * @param httpStatus the http status
     * @param message    the message
     * @param errors     the errors
     */
    public ErrorResponse(HttpStatus httpStatus, String message, List<String> errors) {
        this.httpStatus = httpStatus;
        this.message = message;
        this.errors = errors;
    }

    /**
     * Instantiates a new Error response.
     *
     * @param httpStatus the http status
     * @param message    the message
     * @param error      the error
     */
    public ErrorResponse(HttpStatus httpStatus, String message, String error) {
        this.httpStatus = httpStatus;
        this.message = message;
        this.errors = Collections.singletonList(error);
    }

}
