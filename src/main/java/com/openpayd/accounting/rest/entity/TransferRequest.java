package com.openpayd.accounting.rest.entity;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * The type Transfer request.
 */
@Data
public class TransferRequest {

    @NotNull
    private Long senderAccountId;
    @NotNull
    private Long receiverAccountId;
    @NotNull
    private BigDecimal amount;
    @NotEmpty
    private String message;

}
