package com.openpayd.accounting.rest.boundary;

import com.openpayd.accounting.business.boundary.ClientService;
import com.openpayd.accounting.business.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clients")
@Validated
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping
    public List<Client> getClients() {
        return clientService.findAll();
    }

    @GetMapping("/{clientId}")
    public Client findClient(@PathVariable Long clientId) {
        return clientService.findById(clientId);
    }

    @PostMapping
    public ResponseEntity<Client> createClient(@RequestBody @Valid Client client) {
        return ResponseEntity.ok(clientService.create(client));
    }

    @DeleteMapping("/{clientId}")
    public void removeClient(@PathVariable Long clientId) {
        clientService.deleteById(clientId);
    }

}
