package com.openpayd.accounting.rest.boundary;

import com.openpayd.accounting.business.boundary.TransactionService;
import com.openpayd.accounting.business.entity.Transaction;
import com.openpayd.accounting.rest.entity.TransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/transactions")
@Validated
public class TransferController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping
    public Transaction makeTransaction(@RequestBody @Valid TransferRequest transferRequest) {
        return transactionService.makeTransfer(
                transferRequest.getSenderAccountId(),
                transferRequest.getReceiverAccountId(),
                transferRequest.getAmount(),
                transferRequest.getMessage()
        );
    }

}
